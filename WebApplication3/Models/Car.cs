﻿using System;
using System.Collections.Generic;

namespace WebApplication3.Models
{
    public partial class Car
    {
        public int Id { get; set; }
        public string? Brand { get; set; }
        public string? Model { get; set; }
        public int? Year { get; set; }
    }
}
