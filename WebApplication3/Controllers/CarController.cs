﻿using Microsoft.AspNetCore.Mvc;
using WebApplication3.Models;

// For more information on enabling Web API for empty projects, visit https://go.microsoft.com/fwlink/?LinkID=397860

namespace WebApplication3.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class CarController : ControllerBase
    {
        private ExampleContext dbContext = new ExampleContext();        

        // GET api/<ValuesController>/5
        [HttpGet]
        public List<Car> Get()
        {
            return dbContext.Cars.ToList();
        }

        // POST api/<ValuesController>
        [HttpPost]
        public void Post([FromBody] Car car)
        {
            if (!dbContext.Cars.Contains(car))
            {
                dbContext.Cars.Add(car);
                dbContext.SaveChanges();
            }            
        }               
    }
}
